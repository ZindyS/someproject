package com.example.myapplication

data class X2(
    val description: String,
    val director: String,
    val images: List<String>,
    val name: String,
    val preview: String,
    val runtime: Int,
    val stars: List<String>,
    val year: Int
)