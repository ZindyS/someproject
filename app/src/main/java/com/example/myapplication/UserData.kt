package com.example.myapplication

data class UserData(
    val chatlists: List<Chatlists>,
    val compilationslist: List<Int>,
    val firstName: String,
    val lastName: String,
    val password: String,
    val usermovies: Usermovies
)