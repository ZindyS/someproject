package com.example.myapplication

data class MovieData(
    val age: Int,
    val description: String,
    val episodes: Episodes,
    val images: List<String>,
    val name: String,
    val poster: String,
    val tags: Tags
)