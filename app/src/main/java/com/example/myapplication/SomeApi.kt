package com.example.myapplication

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface SomeApi {
    @GET("https://mydayproject-bda06.firebaseio.com/response/userlists/{email}.json")
    fun call(@Path("email") email:String) : Call<UserData>

    @GET("https://mydayproject-bda06.firebaseio.com/response/movieslist/filterslist.json")
    fun call2() : Call<Filterslist>

    @GET("https://mydayproject-bda06.firebaseio.com/response/movieslist/{id}.json")
    fun call3(@Path("id") id:String) : Call<MovieData>
}