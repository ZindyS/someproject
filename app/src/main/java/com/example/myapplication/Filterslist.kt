package com.example.myapplication

data class Filterslist(
    val forMe: List<Int>,
    val inTrend: List<Int>,
    val new: List<Int>
)