package com.example.myapplication

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onNextClicked(v:View) {
        if (!TextUtils.isEmpty(email.text) && !TextUtils.isEmpty(password.text)) {
            var booba = false

            var str = ""
            var int = -2

            for (i in email.text.toString()) {
                if (i == '@' && !booba) {
                    booba = true
                    str += "_"
                } else if (i == '@' && booba) {
                    booba = false
                    break
                } else if (i == '.' && int == -2) {
                    int++
                    str += "_"
                } else if (i == '.' && int != -2) {
                    booba = false
                    break
                } else if (i in '0'..'9' && int != -2) {
                    booba = false
                    break
                } else if ((i > 'z' || i < 'a') && i !in '0'..'9') {
                    booba = false
                    break
                } else {
                    str += i
                }

                if (int != -2) {
                    int++
                }
            }

            if (booba && int > 0 && int <= 3) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://mydayproject-bda06.firebaseio.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

                val api = retrofit.create(SomeApi::class.java)

                api.call(str).enqueue(object : Callback<UserData> {
                    override fun onResponse(call: Call<UserData>?, response: Response<UserData>?) {
                        if (response!!.isSuccessful) {
                            if (response.body() != null) {
                                if (response.body()!!.password == password.text.toString()) {
                                    val ed = getSharedPreferences("WorldCinema", 0).edit()
                                    ed.putString("email", str)
                                    ed.commit()

                                    val intent = Intent(this@MainActivity, WallActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    startActivity(intent)
                                    finish()
                                } else {
                                    Toast.makeText(baseContext, "Неправильный логин или пароль", Toast.LENGTH_SHORT).show()
                                }
                            } else {
                                Toast.makeText(baseContext, "Неправильный логин или пароль", Toast.LENGTH_SHORT).show()
                            }
                        } else {
                            Toast.makeText(baseContext, "Неправильный логин или пароль", Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onFailure(call: Call<UserData>?, t: Throwable?) {
                        Toast.makeText(baseContext, "Неправильный логин или пароль", Toast.LENGTH_SHORT).show()
                    }

                })
            } else {
                Toast.makeText(this, "Проверьте корректность e-mail'а", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Проверьте поля", Toast.LENGTH_SHORT).show()
        }
    }
}