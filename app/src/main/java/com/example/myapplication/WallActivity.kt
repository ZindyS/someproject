package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_wall.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class WallActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val retrofit = Retrofit.Builder()
            .baseUrl("https://mydayproject-bda06.firebaseio.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(SomeApi::class.java)

        api.call(getSharedPreferences("WorldCinema", 0).getString("email", "")!!).enqueue(object : Callback<UserData> {
            override fun onResponse(call: Call<UserData>?, response: Response<UserData>?) {
                if (response!!.isSuccessful) {
                    textView.text = "${response.body()!!.firstName} ${response.body()!!.lastName}"
                } else {
                    Toast.makeText(baseContext, "Ошибка: ${response!!.message()}", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<UserData>?, t: Throwable?) {
                Toast.makeText(baseContext, "Ошибка: ${t!!.message}", Toast.LENGTH_SHORT).show()
            }
        })

        api.call2().enqueue(object : Callback<Filterslist> {
            override fun onResponse(call: Call<Filterslist>?, response: Response<Filterslist>?) {
                if (response!!.isSuccessful) {
                    val list1 = mutableListOf<MovieData>()
                    val list2 = mutableListOf<MovieData>()
                    val list3 = mutableListOf<MovieData>()
                    for (i in response.body()!!.inTrend) {
                        api.call3(i.toString()).enqueue(object : Callback<MovieData> {
                            override fun onResponse(
                                call: Call<MovieData>?,
                                response: Response<MovieData>?
                            ) {
                                if (response!!.isSuccessful) {
                                    list1.add(response.body()!!)
                                    val adapter = AdapterForRecyclerView()
                                    adapter.setData(list1)

                                    recyclerView1.adapter = adapter

                                    recyclerView1.isFocusable = true
                                } else {
                                    Toast.makeText(baseContext, "Ошибка: ${response!!.message()}", Toast.LENGTH_SHORT).show()
                                }
                            }

                            override fun onFailure(call: Call<MovieData>?, t: Throwable?) {
                                Toast.makeText(baseContext, "Ошибка: ${response!!.message()}", Toast.LENGTH_SHORT).show()
                            }
                        })
                    }

                    for (i in response.body()!!.new) {
                        api.call3(i.toString()).enqueue(object : Callback<MovieData> {
                            override fun onResponse(
                                call: Call<MovieData>?,
                                response: Response<MovieData>?
                            ) {
                                if (response!!.isSuccessful) {
                                    list2.add(response.body()!!)
                                    val adapter = AdapterForRecyclerView()
                                    adapter.setData(list2)

                                    recyclerView2.adapter = adapter

                                    recyclerView2.isFocusable = true
                                } else {
                                    Toast.makeText(baseContext, "Ошибка: ${response!!.message()}", Toast.LENGTH_SHORT).show()
                                }
                            }

                            override fun onFailure(call: Call<MovieData>?, t: Throwable?) {
                                Toast.makeText(baseContext, "Ошибка: ${response!!.message()}", Toast.LENGTH_SHORT).show()
                            }
                        })
                    }

                    for (i in response.body()!!.forMe) {
                        api.call3(i.toString()).enqueue(object : Callback<MovieData> {
                            override fun onResponse(
                                call: Call<MovieData>?,
                                response: Response<MovieData>?
                            ) {
                                if (response!!.isSuccessful) {
                                    list3.add(response.body()!!)
                                    val adapter = AdapterForRecyclerView()
                                    adapter.setData(list3)

                                    recyclerView3.adapter = adapter

                                    recyclerView3.isFocusable = true
                                } else {
                                    Toast.makeText(baseContext, "Ошибка: ${response!!.message()}", Toast.LENGTH_SHORT).show()
                                }
                            }

                            override fun onFailure(call: Call<MovieData>?, t: Throwable?) {
                                Toast.makeText(baseContext, "Ошибка: ${response!!.message()}", Toast.LENGTH_SHORT).show()
                            }
                        })
                    }
                } else {
                    Toast.makeText(baseContext, "Ошибка: ${response!!.message()}", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<Filterslist>?, t: Throwable?) {
                Toast.makeText(baseContext, "Ошибка: ${t!!.message}", Toast.LENGTH_SHORT).show()
            }
        })

        setContentView(R.layout.activity_wall)
    }
}