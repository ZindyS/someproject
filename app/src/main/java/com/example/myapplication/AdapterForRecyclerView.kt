package com.example.myapplication

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.Transformation
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.item.view.*

class AdapterForRecyclerView : RecyclerView.Adapter<AdapterForRecyclerView.VH>() {

    private lateinit var list: MutableList<MovieData>

    fun setData(list: MutableList<MovieData>) {
        this.list = list
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.imageofep
        val text = itemView.nameofep
        val arrow = itemView.imageView4
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.itemView.isFocusable = true
        if (position == list.size) {
            holder.image.visibility = View.INVISIBLE
            holder.text.visibility = View.INVISIBLE
            holder.arrow.visibility = View.VISIBLE

            holder.itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    holder.arrow.animate()
                        .setDuration(200)
                        .scaleX(1.2f)
                        .scaleY(1.2f)
                        .start()
                } else {
                    holder.arrow.animate()
                        .setDuration(200)
                        .scaleX(1f)
                        .scaleY(1f)
                        .start()
                }
            }
        } else {
            Glide.with(holder.image)
                    .asBitmap()
                    .load(Uri.parse("http://cinema.areas.su/up/images/${list[position].poster}"))
                    .apply(RequestOptions().transform(RoundedCorners(20)))
                    .into(holder.image)

            holder.text.text = list[position].name

            holder.itemView.setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    holder.image.animate()
                            .setDuration(200)
                            .scaleX(1.1f)
                            .scaleY(1.1f)
                            .start()
                } else {
                    holder.image.animate()
                            .setDuration(200)
                            .scaleX(1f)
                            .scaleY(1f)
                            .start()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size+1
    }
}